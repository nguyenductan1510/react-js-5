import { dataShoe } from "../../Data_Shoe";
import * as types from "../constants/shoeContants";
let innitialValue = {
  listShoe: dataShoe,
  cart: [],
};

export const shoeReducer = (state = innitialValue, action) => {
  switch (action.type) {
    case types.ADD_TO__CART: {
      let cloneCart = [...state.cart];

      let index = cloneCart.findIndex((item) => {
        return item.id === action.payload.id;
      });

      if (index == -1) {
        let newShoe = { ...action.payload, soLuong: 1 };

        cloneCart.push(newShoe);
      } else {
        cloneCart[index].soLuong += 1;
      }

      return { ...state, cart: cloneCart };
    }
    case types.DELETE_ITEM: {
      let newCart = state.cart.filter((item) => {
        return item.id != action.payload;
      });
      return { ...state, cart: newCart };
    }
    default:
      return state;
  }
};
