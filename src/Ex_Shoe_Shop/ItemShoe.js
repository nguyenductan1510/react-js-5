import { connect } from "react-redux";
import { addToCartAction } from "./redux/action/shoeAction";

import React, { Component } from "react";

class ItemShoe extends Component {
  render() {
    let { image, name, price } = this.props.item;
    return (
      <div className="col-3 p-4">
        <div className="card ">
          <img src={image} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <h5>{price}$</h5>
            <a
              href="#"
              onClick={() => {
                this.props.hendLePushToCart(this.props.item);
              }}
              className="btn btn-primary"
            >
              Add to carts
            </a>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    hendLePushToCart: (shoe) => {
      dispatch(addToCartAction(shoe));
    },
  };
};
export default connect(null, mapDispatchToProps)(ItemShoe);
