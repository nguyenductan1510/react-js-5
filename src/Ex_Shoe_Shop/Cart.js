import React, { Component } from "react";
import { connect } from "react-redux";
import { DELETE_ITEM } from "./redux/constants/shoeContants";

class Cart extends Component {
  renderTbody = () => {
    console.log(this.props.cart);
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <button className="btn btn-danger">-</button>
            <strong className="mx-3">{item.soLuong}</strong>
            <button className="btn btn-success">+</button>
          </td>
          <td>{item.price * item.soLuong}$</td>
          <td>
            <img src={item.image} style={{ width: 50 }} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.hendLeDelete(item.id);
              }}
              className="btn btn-danger "
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Img</th>
            <th>action</th>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    hendLeDelete: (id) => {
      let action = {
        type: DELETE_ITEM,
        payload: id,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
