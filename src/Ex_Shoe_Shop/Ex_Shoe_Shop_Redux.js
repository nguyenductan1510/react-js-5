import React, { Component } from "react";
import ListShoe from "./ListShoe";
import Cart from "./Cart";
export default class Ex_Shoe_Shop_Redux extends Component {
  state = {
    listShoe: [],
    cart: [],
  };
  render() {
    return (
      <div className="container">
        <Cart />

        <ListShoe />
      </div>
    );
  }
}
