import "./App.css";
import Ex_Shoe_Shop_Redux from "./Ex_Shoe_Shop/Ex_Shoe_Shop_Redux";

function App() {
  return (
    <div className="App">
      <Ex_Shoe_Shop_Redux />
    </div>
  );
}

export default App;
