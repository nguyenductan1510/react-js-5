import { ADD_TO__CART } from "../constants/shoeContants";

export const addToCartAction = (shoe) => {
  return {
    type: ADD_TO__CART,
    payload: shoe,
  };
};
